//
//  main.cpp
//  ExpBSystemOSX
//
//  Created by tomomi on 2016/10/20.
//  Copyright (c) 2016年 friendsea. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <iostream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <time.h>

#define MARGEMARGIN 3

using namespace std;
using namespace cv;

Mat BalloonImg;

class Node
{
public:
    Node(int x, int y);
    int Chains = 0;
    cv::Point Position;
    //Node* Parent = NULL;
    //vector<Node*> Child;
    //void Print(Mat image);
    bool isBorder = false;
};

Node::Node(int x, int y)
{
    Position.x = x;
    Position.y = y;
}

/*
void Node::Print(Mat image) {
    if (Chains == 2)
        circle(image, Position, 2, Scalar(255, 0, 0));
    else
        circle(image, Position, 2, Scalar(0, 0, 255));
    if (Parent != NULL)
        line(image, Parent->Position, Position, Scalar(0, 255, 0));
    for (Node* n : Child)n->Print(image);
}
*/


void SetNeighbor(Mat src, int x, int y, int pixels[]) {
    pixels[0] = src.data[src.elemSize()*(x - 1) + src.step*(y - 1)] != 0;
    pixels[1] = src.data[src.elemSize()*(x - 1) + src.step*(y + 0)] != 0;
    pixels[2] = src.data[src.elemSize()*(x - 1) + src.step*(y + 1)] != 0;
    pixels[3] = src.data[src.elemSize()*(x + 0) + src.step*(y + 1)] != 0;
    pixels[4] = src.data[src.elemSize()*(x + 1) + src.step*(y + 1)] != 0;
    pixels[5] = src.data[src.elemSize()*(x + 1) + src.step*(y + 0)] != 0;
    pixels[6] = src.data[src.elemSize()*(x + 1) + src.step*(y - 1)] != 0;
    pixels[7] = src.data[src.elemSize()*(x + 0) + src.step*(y - 1)] != 0;
}

Point FirstPoint(Mat map) {
    for (int x = 1; x < map.cols - 2; x++)
        for (int y = 1; y < map.rows - 2; y++)
            if (map.data[map.step*y + map.elemSize()*x])
                return Point(x, y);
    return Point(0, 0);
}

bool ThinPixel2(Mat src, int x, int y, bool conditionchange) {
    if (src.data[src.elemSize()*x + src.step*y] == 0)return false;
    int pixels[8];
    SetNeighbor(src, x, y, pixels);
    int n = 0, d = 0;
    for (int i = 0; i < 8; i++) {
        n += pixels[i];
        d += (!pixels[i]) && pixels[(i + 1) % 8];
    }
    if (n < 2)return false;
    if (n > 6)return false;
    if (d != 1)return false;
    if (pixels[1] * pixels[3] * pixels[5 + conditionchange * 2])return false;
    if (pixels[3 - conditionchange * 2] * pixels[5] * pixels[7])return false;
    return true;
}

void Thin2(Mat src) {
    bool flag = false;
    Mat deletelist = Mat::zeros(src.rows, src.cols, CV_8U);
    for (int x = 1; x < src.cols - 1; x++)
        for (int y = 1; y < src.rows - 1; y++)
            flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, false));
    src = src - deletelist;
    if (!flag)return;
    
    deletelist = Scalar::all(0);
    for (int x = src.cols - 2; x > 0; x--)
        for (int y = src.rows - 2; y > 0; y--)
            flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, true));
    src = src - deletelist;
    if (flag)Thin2(src);
}

//vector<Node*> Nodes;
vector<Node*> ChainHeads;

bool Trace(Point pt, Mat* map, Node* node) {
    if (map->data[map->step*pt.y + map->elemSize()*pt.x] == 0)
        return false;
    map->data[map->step*pt.y + map->elemSize()*pt.x] = 0;
    Node* newnode = new Node(pt.x, pt.y);
    //Nodes.push_back(newnode);
    if (node != NULL) {
        node->Child.push_back(newnode);
        newnode->Parent = node;
    }
    else ChainHeads.push_back(newnode);
    newnode->Chains = node != NULL;
    newnode->Chains += Trace(Point(pt.x + 1, pt.y), map, newnode);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y), map, newnode);
    newnode->Chains += Trace(Point(pt.x, pt.y + 1), map, newnode);
    newnode->Chains += Trace(Point(pt.x, pt.y - 1), map, newnode);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y - 1), map, newnode);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y + 1), map, newnode);
    newnode->Chains += Trace(Point(pt.x + 1, pt.y + 1), map, newnode);
    newnode->Chains += Trace(Point(pt.x + 1, pt.y - 1), map, newnode);
    if (newnode->Chains == 0){
        delete newnode;
        ChainHeads.resize(ChainHeads.size()-1);
    }
    return true;
}

Node* GetForwardPath(Node* node, int n) {
    if (n == 0)return node;
    if (node->Chains != 2)return node;
    return GetForwardPath(node->Child[0], n - 1);
}

void ReducePath(Node* node) {
    //if (node->Type == Non)return;
    if (node->Chains != 2 ||
        node->Parent == NULL //||
        //currntlength >= 10
        ) {
        for (int i = 0; i < node->Child.size(); i++) {
            //if (node->Chain[i] == beforenode)continue;
            ReducePath(node->Child[i]);
        }
        return;
    }
    
    Point v1 = node->Position - node->Parent->Position;
    Point v2 = GetForwardPath(node, 5)->Position - node->Parent->Position;
    double curve = abs(v1.x*v2.y - v1.y*v2.x) / norm(v2);
    if (curve > 1.1) {
        ReducePath(node->Child[0]);
        return;
    }
    
    for (int i = 0; i < node->Parent->Child.size(); i++)
        if (node->Parent->Child[i] == node)
            node->Parent->Child[i] = node->Child[0];
    node->Child[0]->Parent = node->Parent;
    ReducePath(node->Child[0]);
    delete node;
}

void ScaleNode(Node* node, int scale, int x, int y){
    node->Position*=scale;
    node->Position+=Point(x,y);
    for(Node* n : node->Child)
        ScaleNode(n, scale,x,y);
}

class Segment
{
public:
    Segment(Node* head, int childIndex);
    ~Segment();
    int ChildIndex;
    Node* Head;
    Node* End;
    Rect AABB;
    //bool Collision(Segment* target);
    void Print(Mat image);
private:
    
};

Segment::Segment(Node* head, int childIndex)
{
    AABB = Rect(head->Position.x-MARGEMARGIN, head->Position.y-MARGEMARGIN,0,0);
    Head = head;
    ChildIndex=childIndex;
    End = Head->Child[childIndex];
    int aabbrx=AABB.x + MARGEMARGIN*2,aabbby=AABB.y+ MARGEMARGIN*2;
    while (1) {
        AABB.x = MIN(AABB.x, End->Position.x - MARGEMARGIN);
        AABB.y = MIN(AABB.y, End->Position.y - MARGEMARGIN);
        aabbrx = MAX(aabbrx, End->Position.x + MARGEMARGIN);
        aabbby = MAX(aabbby, End->Position.y + MARGEMARGIN);
        if(End->Chains != 2) break;
        End=End->Child[0];
    }
    AABB.width = aabbrx-AABB.x;
    AABB.height = aabbby - AABB.y;
}

Segment::~Segment()
{
}

void Segment::Print(Mat image){
    //rectangle(image, AABB, Scalar(0,0,255));
    Node* node = Head->Child[ChildIndex];
    int r = rand()%155+100;
    int g = rand()%155+100;
    int b = rand()%155+100;
    while (1) {
        line(image, node->Parent->Position, node->Position, Scalar(b,g,r));
        if(node->Chains == 1)circle(image, node->Position, 2, Scalar(255,0,0));
        if(node->Chains == 2)circle(image, node->Position, 2, Scalar(0,0,255));
        if(node->Chains == 3)circle(image, node->Position, 2, Scalar(255,0,255));
        if(node == End)break;
        node = node->Child[0];
    }
}


vector<Segment*> Segments;

void MakeSegment(Node* head){
    for (int i = 0; i < head->Child.size(); i++) {
        Segment* newSegment = new Segment(head, i);
        MakeSegment(newSegment->End);
        Segments.push_back(newSegment);
    }
}

class Group{
public:
    Group(Segment* segement);
    //void Add(Segment* segment);
    void Marge(Group* target);
    bool Collision(Group* target);
    //void MakePath();
    //void CheckBorder();
    void Print(Mat image);
private:
    vector<Segment*> Segments;
    //vector<Point*> BorderPath;
    Rect BB;
};

Group::Group(Segment* segment){
    BB = Rect(segment->AABB);
    Segments.push_back(segment);
}

void Group::Marge(Group *target){
    int bbrx = BB.x+BB.width;
    int bbby = BB.y+BB.height;
    BB.x = MIN(BB.x, target->BB.x);
    BB.y = MIN(BB.y, target->BB.y);
    bbrx = MAX(bbrx, target->BB.x+target->BB.width);
    bbby = MAX(bbby, target->BB.y+target->BB.height);
    BB.width = bbrx - BB.x;
    BB.height = bbby - BB.y;
    for(Segment* seg : target->Segments)
        Segments.push_back(seg);
    delete target;
}

bool Group::Collision(Group *target){
    if(BB.x+BB.width < target->BB.x)return false;
    if(BB.y+BB.height< target->BB.y)return false;
    if(BB.x > target->BB.x+target->BB.width)return false;
    if(BB.y > target->BB.y+target->BB.height)return false;
    return true;
}

void Group::Print(Mat image){
    rectangle(image, BB, Scalar(255,0,255));
}
/*
 void Group::MakePath(){
 BorderPath.clear();
 BorderPath.push_back(new Point(BB.x,BB.y));
 BorderPath.push_back(new Point(BB.x+BB.width,BB.y));
 BorderPath.push_back(new Point(BB.x+BB.width,BB.y+BB.height));
 BorderPath.push_back(new Point(BB.x,BB.y+BB.height));
 }
 */
vector<Group*> Groups;

bool MargeGroups(){
    bool ret = false;
    for(int i = 0; i < Groups.size(); i++){
        if(Groups[i] == NULL)continue;
        for(int j = 0; j < Groups.size(); j++){
            if(Groups[j] == NULL)continue;
            if(i == j)continue;
            if(Groups[i]->Collision(Groups[j])){
                Groups[i]->Marge(Groups[j]);
                Groups[j] = NULL;
                ret = true;
            }
        }
    }
    return ret;
}



int main(int argc, char *argv[]) {
    srand(clock());
    const char* filename = argc >= 2 ? argv[1] : "/Users/tomomi/ExpBSystem/nyan.jpg";
    //load image and invert
    Mat Source = imread(filename, 1);
    if (Source.empty()) {
        std::cout << "Source is empty" << endl;
        return -1;
    }
    cvtColor(Source, Source, COLOR_BGRA2GRAY);
    Source = cv::Scalar::all(255) - Source;
    //make eroded image
    Mat eroded;
    erode(Source, eroded, Mat(7, 7, CV_8U, cv::Scalar(1)));
    blur(eroded, eroded, Size(5, 5));
    //binalize
    Source = Source - eroded;
    threshold(Source, Source, 30, 1, CV_THRESH_BINARY);
    /*
     //make border source
     Mat BorderSource;
     Source.copyTo(BorderSource);
     dilate(BorderSource, BorderSource, Mat(20, 20, CV_8U, cv::Scalar(1)));
     BorderSource = Scalar::all(1)-BorderSource;
     //Canny(BorderSource, BorderSource, 1, 1);
     cv::imshow("border", BorderSource*255);
     */
    //thinning
    Thin2(Source);
    //trace and make nodes
    Mat* clonemap = new Mat();
    Source.copyTo(*clonemap);
    while (Trace(FirstPoint(*clonemap), clonemap, NULL));
    //erase unnecessary nodes
    for (int i = 0; i < ChainHeads.size(); i++)
        ReducePath(ChainHeads[i]);
    //make vector segments
    for (int i = 0; i < ChainHeads.size(); i++)
        MakeSegment(ChainHeads[i]);
    //make groups
    for(Segment* seg : Segments)
        Groups.push_back(new Group(seg));
    while(MargeGroups());
    
    //--------shaw images---------------------
    Source = Source.mul(255);
    cvtColor(Source, Source, COLOR_GRAY2BGR);
    
    Mat vecimage = Mat::zeros(Source.rows, Source.cols, CV_8UC3);
    for(Segment* g : Segments)
        g->Print(vecimage);
    for(Group* gr : Groups)
        if(gr != NULL)
            gr->Print(vecimage);
    
    cv::namedWindow("image", CV_WINDOW_AUTOSIZE | CV_WINDOW_FREERATIO);
    cv::imshow("image", Source);
    cv::imshow("vector", vecimage);
    cv::waitKey(0);
    return 0;
}
