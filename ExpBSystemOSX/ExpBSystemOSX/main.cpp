#include <opencv2/opencv.hpp>
#include <iostream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <time.h>

using namespace std;
using namespace cv;

Mat BalloonImg;

void DrawTransPinP(cv::Mat &img_dst, const cv::Mat hukidasiImg, const cv::Mat baseImg, vector<cv::Point2f> tgtPt, int pos)
{
    /*
    Mat baseImg = Mat::zeros(baseBin.rows, baseBin.cols, CV_8UC3);
    cout <<"baseBin:"<< baseBin.channels()<<endl;
    cvtColor(baseBin, baseImg, CV_GRAY2RGB);
     */
    //cv::Mat img_rgb, img_aaa, img_1ma;
    //vector<cv::Mat>planes_rgba, planes_rgb, planes_aaa, planes_1ma;
    //int maxVal = pow(2, 8 * baseImg.elemSize1()) - 1;
    Mat transImg = hukidasiImg.clone();
    
    //吹き出し画像を位置によって反転する
    if (pos == 0)
        flip(hukidasiImg, transImg, 1);
    else if (pos == 11)
        flip(hukidasiImg, transImg, 0);
    else if (pos == 10)
        flip(hukidasiImg, transImg, -1);
    
    /*
    //透過画像はRGBA, 背景画像はRGBのみ許容。ビット深度が同じ画像のみ許容
    if (transImg.data == NULL || baseImg.data == NULL || transImg.channels()<4 || baseImg.channels()<3 || (transImg.elemSize1() != baseImg.elemSize1()))
    {
        img_dst = cv::Mat(baseImg.rows , baseImg.cols, CV_8UC3);
        img_dst = cv::Scalar::all(maxVal);
        return;
    }
    
    //書き出し先座標が指定されていない場合は背景画像の中央に配置する
    if (tgtPt.size()<4)
    {
        //座標指定(背景画像の中心に表示する）
        int ltx = (baseImg.cols - transImg.cols) / 2;
        int lty = (baseImg.rows - transImg.rows) / 2;
        int ww = transImg.cols;
        int hh = transImg.rows;
        
        tgtPt.push_back(cv::Point2f(ltx, lty));
        tgtPt.push_back(cv::Point2f(ltx + ww, lty));
        tgtPt.push_back(cv::Point2f(ltx + ww, lty + hh));
        tgtPt.push_back(cv::Point2f(ltx, lty + hh));
    }
     */
    
    //変形行列を作成
    vector<cv::Point2f>srcPt;
    srcPt.push_back(cv::Point2f(0, 0));
    srcPt.push_back(cv::Point2f(transImg.cols - 1, 0));
    srcPt.push_back(cv::Point2f(transImg.cols - 1, transImg.rows - 1));
    srcPt.push_back(cv::Point2f(0, transImg.rows - 1));
    cv::Mat mat = cv::getPerspectiveTransform(srcPt, tgtPt);
    
    //出力画像と同じ幅・高さのアルファ付き画像を作成
    cv::Mat alpha0(baseImg.rows, baseImg.cols, transImg.type());
    alpha0 = cv::Scalar::all(0);
    cv::warpPerspective(transImg, alpha0, mat, alpha0.size(), cv::INTER_CUBIC, cv::BORDER_TRANSPARENT);
    
    /*
    //チャンネルに分解
    cv::split(alpha0, planes_rgba);
    
    //RGBA画像をRGBに変換
    planes_rgb.push_back(planes_rgba[0]);
    planes_rgb.push_back(planes_rgba[1]);
    planes_rgb.push_back(planes_rgba[2]);
    merge(planes_rgb, img_rgb);
    
    //RGBA画像からアルファチャンネル抽出
    planes_aaa.push_back(planes_rgba[3]);
    planes_aaa.push_back(planes_rgba[3]);
    planes_aaa.push_back(planes_rgba[3]);
    merge(planes_aaa, img_aaa);
    
    //背景用アルファチャンネル
    planes_1ma.push_back(maxVal - planes_rgba[3]);
    planes_1ma.push_back(maxVal - planes_rgba[3]);
    planes_1ma.push_back(maxVal - planes_rgba[3]);
    merge(planes_1ma, img_1ma);
    
    img_dst = img_rgb.mul(img_aaa, 1.0 / (double)maxVal) + baseImg.mul(img_1ma, 1.0 / (double)maxVal);
    cvtColor(img_dst, img_dst, CV_RGB2GRAY);
     */
    img_dst = baseImg+alpha0;
}

class Node
{
public:
    Node(int x, int y);
    int Chains = 0;
    Point2f Position;
    Node* Parent = NULL;
    vector<Node*> Child;
    void Print(Mat image);
    double curve = 0;
};

Node::Node(int x, int y)
{
    Position.x = x;
    Position.y = y;
}

void Node::Print(Mat image) {
    if (Chains == 2)
        circle(image, Position, 2, Scalar(255, 0, 0));
    else
        circle(image, Position, 2, Scalar(0, 0, 255));
    if (Parent != NULL)
        line(image, Parent->Position, Position, Scalar(0, 255, 0));
    for (Node* n : Child)n->Print(image);
}


void SetNeighbor(Mat src, int x, int y, int pixels[]) {
    pixels[0] = src.data[src.elemSize()*(x - 1) + src.step*(y - 1)] != 0;
    pixels[1] = src.data[src.elemSize()*(x - 1) + src.step*(y + 0)] != 0;
    pixels[2] = src.data[src.elemSize()*(x - 1) + src.step*(y + 1)] != 0;
    pixels[3] = src.data[src.elemSize()*(x + 0) + src.step*(y + 1)] != 0;
    pixels[4] = src.data[src.elemSize()*(x + 1) + src.step*(y + 1)] != 0;
    pixels[5] = src.data[src.elemSize()*(x + 1) + src.step*(y + 0)] != 0;
    pixels[6] = src.data[src.elemSize()*(x + 1) + src.step*(y - 1)] != 0;
    pixels[7] = src.data[src.elemSize()*(x + 0) + src.step*(y - 1)] != 0;
}

Point FirstPoint(Mat map) {
    for (int x = 1; x < map.cols - 2; x++)
        for (int y = 1; y < map.rows - 2; y++)
            if (map.data[map.step*y + map.elemSize()*x])
                return Point(x, y);
    return Point(0, 0);
}

bool ThinPixel2(Mat src, int x, int y, bool conditionchange) {
    if (src.data[src.elemSize()*x + src.step*y] == 0)return false;
    int pixels[8];
    SetNeighbor(src, x, y, pixels);
    int n = 0, d = 0;
    for (int i = 0; i < 8; i++) {
        n += pixels[i];
        d += (!pixels[i]) && pixels[(i + 1) % 8];
    }
    if (n < 2)return false;
    if (n > 6)return false;
    if (d != 1)return false;
    if (pixels[1] * pixels[3] * pixels[5 + conditionchange * 2])return false;
    if (pixels[3 - conditionchange * 2] * pixels[5] * pixels[7])return false;
    return true;
}

void Thin2(Mat src) {
    bool flag = false;
    Mat deletelist = Mat::zeros(src.rows, src.cols, CV_8U);
    for (int x = 1; x < src.cols - 1; x++)
        for (int y = 1; y < src.rows - 1; y++)
            flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, false));
    src = src - deletelist;
    if (!flag)return;
    
    deletelist = Scalar::all(0);
    for (int x = src.cols - 2; x > 0; x--)
        for (int y = src.rows - 2; y > 0; y--)
            flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, true));
    src = src - deletelist;
    if (flag)Thin2(src);
}

//vector<Node*> ChainHeads;

bool Trace(Point2f pt, Mat* map, Node* node, vector<Node*> *ChainHeads, Mat* Original) {
    if(pt.x<0||pt.y<0)return false;
    if (map->data[map->step*(int)pt.y + map->elemSize()*(int)pt.x] == 0)
        return false;
    map->data[map->step*(int)pt.y + map->elemSize()*(int)pt.x] = 0;
    Node* newnode = new Node(pt.x, pt.y);
    //Nodes.push_back(newnode);
    if (node != NULL) {
        node->Child.push_back(newnode);
        newnode->Parent = node;
    }
    else ChainHeads->push_back(newnode);
    newnode->Chains = node != NULL;
    if(node != NULL)newnode->Chains += Trace(pt*2-node->Position, map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x + 1, pt.y), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x, pt.y + 1), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x, pt.y - 1), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y - 1), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x - 1, pt.y + 1), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x + 1, pt.y + 1), map, newnode, ChainHeads, Original);
    newnode->Chains += Trace(Point(pt.x + 1, pt.y - 1), map, newnode, ChainHeads, Original);
    if (newnode->Chains == 0){
        delete newnode;
        ChainHeads->resize(ChainHeads->size()-1);
    }
    if(newnode->Chains == 1)return true;
    if(Original == NULL)return true;
    int branch = 0;
    int pixels[8];
    SetNeighbor(*Original, pt.x, pt.y, pixels);
    for(int i = 0; i < 8; i+=2)
        branch+=pixels[(i+7)%8] ==0 && pixels[i]!=0 && pixels[(i+1)%8] == 0;
    for(int i = 1; i < 8; i+=2)
        branch+=pixels[i] != 0;
    if(branch > newnode->Chains)newnode->Chains = branch;
    return true;
}

Node* GetForwardPath(Node* node, int n) {
    if (n == 0)return node;
    if (node->Chains != 2)return node;
    return GetForwardPath(node->Child[0], n - 1);
}

void ReducePath(Node* node) {
    //if (node->Type == Non)return;
    if (node->Chains != 2 ||
        node->Parent == NULL //||
        //currntlength >= 10
        ) {
        for (int i = 0; i < node->Child.size(); i++) {
            //if (node->Chain[i] == beforenode)continue;
            ReducePath(node->Child[i]);
        }
        return;
    }

    Point v1 = node->Position - node->Parent->Position;
    Point v2 = GetForwardPath(node, 5)->Position - node->Parent->Position;
    node->curve = abs(v1.x*v2.y - v1.y*v2.x) / norm(v2);
    if (node->curve > 1.1) {
        ReducePath(node->Child[0]);
        return;
    }

    for (int i = 0; i < node->Parent->Child.size(); i++)
        if (node->Parent->Child[i] == node)
            node->Parent->Child[i] = node->Child[0];
    node->Child[0]->Parent = node->Parent;
    ReducePath(node->Child[0]);
    delete node;
}

class Segment
{
public:
    Segment(Node* head, int childIndex);
    ~Segment();
    int ChildIndex;
    Node* Head;
    Node* End;
    Rect AABB;
    //bool Collision(Segment* target);
    void Print(Mat image, double scale, Point2f offset);
    void PrintLine(Mat image);
    double Curve();
    vector<Point> Points();
    void CheckBorder(Mat image);
    bool isBorder = false;
private:
    
};

Segment::Segment(Node* head, int childIndex)
{
    Head = head;
    ChildIndex=childIndex;
    End = Head->Child[childIndex];
    while (1) {
        if(End->Chains != 2) break;
        End=End->Child[0];
    }
    AABB = boundingRect(Mat(Points()));
}

Segment::~Segment()
{
}

vector<Point> Segment::Points(){
    vector<Point> vec;
    vec.push_back(Head->Position);
    Node* node = Head->Child[ChildIndex];
    while (1) {
        vec.push_back(node->Position);
        if(node == End) break;
        node = node->Child[0];
    }
    return vec;
}

void Segment::Print(Mat image, double scale, Point2f offset){
    //if(isBorder)return;
    Node* node = Head->Child[ChildIndex];
    int r = rand()%155+100;
    int g = rand()%155+100;
    int b = rand()%155+100;
    while (1) {
        line(image, node->Parent->Position*scale+offset, node->Position*scale+offset, Scalar(b,g,r),2);
        if(node->Chains == 1)circle(image, node->Position*scale+offset, 2, Scalar(255,0,0));
        if(node->Chains == 2)circle(image, node->Position*scale+offset, 2, Scalar(0,0,255));
        if(node->Chains == 3)circle(image, node->Position*scale+offset, 2, Scalar(255,0,255));
        if(node == End)break;
        node = node->Child[0];
    }
}

#define ENDGROW 0
void Segment::PrintLine(Mat image){
    Node* node = Head->Child[ChildIndex];
    while (1) {
        line(image, node->Parent->Position, node->Position, Scalar(1));
        if(node == End)break;
        node = node->Child[0];
    }
    Point2d vec = Head->Position-Head->Child[0]->Position;
    vec *= 1.0/norm(vec);
    line(image, Head->Position, Point2d(Head->Position.x,Head->Position.y)+vec*ENDGROW, Scalar(1));
    vec = End->Position - End->Parent->Position;
    vec *= 1.0/norm(vec);
    line(image, End->Position, Point2d(End->Position.x,End->Position.y)+vec*ENDGROW, Scalar(1));
}

double Segment::Curve(){
    Node* node = Head->Child[ChildIndex];
    double curv = 0;
    double len = 0;
    while (1) {
        curv += node->curve;
        len += norm(node->Parent->Position - node->Position);
        if(node == End)break;
        node = node->Child[0];
    }
    if(len < 50) len = 50;
    return curv/len;
}

void Segment::CheckBorder(Mat image){
    if(isBorder)return;
    double outcount=0;
    vector<Point> points = Points();
    for(Point pt : points)
        if(!image.data[image.elemSize()*pt.x+image.step*pt.y])outcount+=1;
    if(outcount/points.size()>0.7)isBorder = true;
}

void MakeSegment(Node* head, vector<Segment*> *Segments){
    for (int i = 0; i < head->Child.size(); i++) {
        Segment* newSegment = new Segment(head, i);
        MakeSegment(newSegment->End, Segments);
        Segments->push_back(newSegment);
    }
}

class Group{
public:
    Group(Segment* segement);
    //void Add(Segment* segment);
    void Marge(Group* target);
    bool Collision(Group* target);
    //void MakePath();
    //void CheckBorder();
    void Print(Mat image);
    vector<Segment*> Segments;
    vector<Point> BorderPath;
    Rect BB;
};

Group::Group(Segment* segment){
    BB = Rect(segment->AABB);
    convexHull(segment->Points(), BorderPath);
    Segments.push_back(segment);
}

void Group::Marge(Group *target){
    for(Segment* seg : target->Segments)
        Segments.push_back(seg);
    BorderPath.insert(BorderPath.end(), target->BorderPath.begin(), target->BorderPath.end());
	vector<Point> newconvex;
    convexHull(BorderPath, newconvex);
	BorderPath = newconvex;
    BB = boundingRect(BorderPath);
    delete target;
}

bool Group::Collision(Group *target){
    if(BB.x+BB.width < target->BB.x)return false;
    if(BB.y+BB.height< target->BB.y)return false;
    if(BB.x > target->BB.x+target->BB.width)return false;
    if(BB.y > target->BB.y+target->BB.height)return false;
    for(Point pt : BorderPath)
        if(pointPolygonTest(target->BorderPath, pt, false)>=0)return true;
    for(Point pt : target->BorderPath)
        if(pointPolygonTest(BorderPath, pt, false)>=0)return true;
    return false;
}

void Group::Print(Mat image){
    polylines(image, BorderPath, true, Scalar(255,0,255));
}

bool MargeGroups(vector<Group*> *Groups){
    bool ret = false;
    for(int i = 0; i < Groups->size(); i++){
        if((*Groups)[i] == NULL)continue;
        for(int j = 0; j < Groups->size(); j++){
            if((*Groups)[j] == NULL)continue;
            if(i == j)continue;
            if((*Groups)[i]->Collision((*Groups)[j])){
                (*Groups)[i]->Marge((*Groups)[j]);
                (*Groups)[j] = NULL;
                ret = true;
            }
        }
    }
    return ret;
}

vector<Point> FormatPath(vector<Point> vec, double width){
    vector<Point> ret;
    
    vector<vector<Point>> points;
    for(int i = 0; i < vec.size(); i++)
    {
        bool flag = false;
        for(int j = 0; j < points.size(); j++){
            if(abs(points[j][0].x-vec[i].x)<width){
                points[j].push_back(vec[i]);
                flag = true;
                break;
            }
        }
        if(flag)continue;
        points.push_back(vector<Point>());
        points[points.size()-1].push_back(vec[i]);
    }
    for(vector<Point> pts : points){
        if(pts.size()<=1)continue;
        double ave = 0, div = 0;
        for(int i = 0; i < pts.size(); i++){
            double l = abs(pts[i].y+pts[(i+1)%pts.size()].y);
            ave+=(pts[i].x+pts[(i+1)%pts.size()].x)/2*l;;
            div += l;
        }
        ave /= div;
        for(int i = 0; i < vec.size(); i++){
            if(abs(pts[0].x-vec[i].x)<width)
                vec[i].x = ave;
        }
    }
    
    points.clear();
    for(int i = 0; i < vec.size(); i++)
    {
        bool flag = false;
        for(int j = 0; j < points.size(); j++){
            if(abs(points[j][0].y-vec[i].y)<width){
                points[j].push_back(vec[i]);
                flag = true;
                break;
            }
        }
        if(flag)continue;
        points.push_back(vector<Point>());
        points[points.size()-1].push_back(vec[i]);
    }
    for(vector<Point> pts : points){
        if(pts.size()<=1)continue;
        double ave = 0, div = 0;
        for(int i = 0; i < pts.size(); i++){
            double l = abs(pts[i].x+pts[(i+1)%pts.size()].x);
            ave+=(pts[i].y+pts[(i+1)%pts.size()].y)/2*l;;
            div += l;
        }
        ave /= div;
        for(int i = 0; i < vec.size(); i++){
            if(abs(pts[0].y-vec[i].y)<width)
                vec[i].y = ave;
        }
    }
    
    return vec;
}

vector<Point> GrowPath(vector<Point> points, double dist){
    vector<Point> ret;
    for(int i = 0; i < points.size(); i++){
        Point2d v1 = points[i] - points[(i+points.size()-1)%points.size()];
        Point2d v2 = points[(i+1)%points.size()] - points[i];
        v1 *= 1.0/norm(v1);
        v2 *= 1.0/norm(v2);
        Point2d v1n = Point2d(v1.y, -v1.x);
        Point2d v2n = Point2d(v2.y, -v2.x);
        Point2d v12n = v1n+v2n;
        v12n *= 1.0/norm(v12n);
        double tangent = sqrt(1/pow(v1n.dot(v12n), 2)-1);
        ret.push_back(Point2d(points[i].x,points[i].y)+v1n*dist+v1*tangent*dist);
    }
    return ret;
}

class Balloon{
public:
    vector<Point2f> Points;
    double StdX;
    double StdY;
    Point2f Avg;
    void SetStd();
    double Distace(Balloon target);
    void Marge(Balloon target);
    Rect BB;
};

void Balloon::SetStd(){
    Mat data = Mat::zeros(Points.size(), 2, CV_32F);
    Avg = Point2f(0,0);
    StdX=0;
    StdY=0;
    for(Point2f pt : Points)
        Avg += pt;
    Avg *= (float)1/Points.size();
    for(Point2f pt : Points){
        StdX+=pow(Avg.x-pt.x, 2);
        StdY+=pow(Avg.y-pt.y, 2);
    }
    StdX *= (float)1/Points.size();
    StdY *= (float)1/Points.size();
    StdX=sqrt(StdX);
    StdY=sqrt(StdY);
}

double Balloon::Distace(Balloon target){
    double stdx = (StdX+target.StdX)/2;
    double stdy = (StdY+target.StdY)/2;
    double x = (target.Avg.x-Avg.x)/stdx;
    double y = (target.Avg.y-Avg.y)/stdy;
    return sqrt(x*x+y*y);
}

void Balloon::Marge(Balloon target){
    for(Point2f pt : target.Points)
        Points.push_back(pt);
    SetStd();
}

void MargeLocalBalloons(vector<Balloon*> *Balloons){
    for(int i = 0; i < Balloons->size(); i++){
        if((*Balloons)[i] == NULL)continue;
        for(int j = 0; j < Balloons->size(); j++){
            if((*Balloons)[j] == NULL)continue;
            if((*Balloons)[i] == (*Balloons)[j])continue;
            if((*Balloons)[i]->Distace(*(*Balloons)[j]) < 4.5){
                (*Balloons)[i]->Marge(*(*Balloons)[j]);
                delete (*Balloons)[j];
                (*Balloons)[j] = NULL;
            }
        }
    }
}

void hukidasi(Mat src_img, vector<Balloon*> *Balloons){
    Balloons->clear();
    //グレースケール
    Mat gray_img, bin_img, result_img;
    cvtColor(src_img, gray_img, CV_BGR2GRAY);
    
    //2値化
    threshold(gray_img, bin_img, 127, 255, THRESH_BINARY_INV | THRESH_OTSU);
    //imshow("bin", bin_img);
    
    //輪郭の座標リスト
    vector< vector< Point > > contours;
    
    //吹き出し検出
    int i, j, gi = 0, gj = 0;
    double conarea, conlength, r, circlerate, uneven;
    vector<  Point >  contours_g;
    vector<Vec4i> hierarchy;
    vector<int> hukidasiid;
    Mat canny_img, canny_ch, op_img;
    Canny(bin_img, canny_img, 50, 200);
    dilate(bin_img, op_img, Mat(), Point(-1, -1), 1);
    erode(op_img, op_img, Mat(), Point(-1, -1), 1);
    findContours(op_img, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
    
    for (int i = 0; i < (int)contours.size(); i++){
        if (hierarchy[i][3] != -1){						//外側の輪郭(コマ割り)は吹き出し検出しない
            conarea = contourArea(contours.at(i));
            conlength = arcLength(contours.at(i), 1);
            r = conlength / 3.14;
            r = r / 2;
            circlerate = (4 * 3.14*conarea) / (conlength*conlength);
            uneven = (conlength*conlength) / (4 * 3.14*conarea);
            
            /*吹き出し検出の形に関する閾値設定*/
            /*circlerate:円形度　　　　　　　 */
            /*conarea   :面積　　　　　　　　 */
            /*fabs～　　:円の面積との比較     */
            /*uneven    :複雑度(凹凸度合い)　 */
            /*以下各吹き出し候補の重心を計算　*/
            if (circlerate > 0.6&& conarea > 300 /*&& fabs(conarea - (r*r*3.14)) < (conarea / 3.2) */ && uneven >= 1.1){
                int count = contours.at(i).size();
                int x = 0; int y = 0;
                for (int j = 0; j < count; j++){
                    x += contours.at(i).at(j).x;
                    y += contours.at(i).at(j).y;
                }
                x /= count;
                y /= count;
                int l;
                for (l = 0; l < contours_g.size(); l++){ //重心が近すぎるものを排除して被りを防ぐ
                    if (fabs(sqrt((x - contours_g.at(l).x)*(x - contours_g.at(l).x) + (y - contours_g.at(l).y)*(y - contours_g.at(l).y))) < conlength / 4)
                        break;
                }
                if (l == contours_g.size()){
                    contours_g.push_back(Point(x, y));
                    hukidasiid.push_back(i);
                }
            }
        }
    }
    
    
    int row, col;
    int canny;
    vector< vector<int> > c_tmp(canny_img.rows, vector<int>(canny_img.cols, 0));
    
    /*エッジの密度を計算*/
    /*pixs×pixsの範囲の中にperパーセント以上エッジが含まれているかを判定*/
    for (row = 0; row<canny_img.rows; row++){
        for (col = 0; col < canny_img.cols; col++){
            canny = canny_img.at<uchar>(row, col);
            if (canny > 127)
                c_tmp[row][col] = 1;
            else
                c_tmp[row][col] = 0;
        }
    }
    int pixs = 20, per = 10;
    int canny_width = canny_img.rows / pixs, canny_height = canny_img.cols / pixs, count = 0;
    for (row = 0; row < canny_width; row++){
        for (col = 0; col < canny_height; col++){
            for (i = row*pixs; i < row*pixs + pixs; i++){
                for (j = col*pixs; j < col*pixs + pixs; j++){
                    if (c_tmp[i][j] == 1)
                        count++;
                }
            }
            /*エッジの密度がper以上の時に, 各吹き出し候補の輪郭の重心が*/
            /*そのエッジの密度が高い部分に含まれるか否かを判定　　　　 */
            /*(吹き出しの中心には文字があって密度が高いと思われるため) */
            /*含まれていればそれは吹き出しだとみなし, 整形の際の吹き出しの大きさを求めるため外接矩形を求める*/
            /*そして元の輪郭を削除する　　　　　　　　　　　　　　　　 */
            if (count >= (pixs*pixs*per / 100)){
                for (gi = 0; gi < (int)hukidasiid.size(); gi++){
                    if (i - pixs < contours_g.at(gi).x&& j - pixs < contours_g.at(gi).y&&contours_g.at(gi).x < i&&contours_g.at(gi).y < j){
                        //rectangle(src_img, Point(i - pixs, j - pixs), Point(i, j), Scalar(0, 0, 200), 3, 4);
                        Rect hrect = boundingRect(contours.at(hukidasiid[gi]));
                        Balloon* newBalloon = new Balloon();
                        newBalloon->BB = hrect;
                        Balloons->push_back(newBalloon);
                        /*
                        //drawContours(src_img, contours, hukidasiid[gi], Scalar(255, 255, 255), 4, CV_AA);
                        
                        //吹き出しが含まれるコマの重心を求め, 吹き出しの重心と比較し
                        //吹き出しの相対位置を求める
                        int p = hierarchy[hukidasiid[gi]][3];
                        int count = contours.at(p).size();
                        int x = 0; int y = 0;
                        for (int j = 0; j < count; j++){
                            x += contours.at(p).at(j).x;
                            y += contours.at(p).at(j).y;
                        }
                        x /= count;
                        y /= count;

                        int pos = 0;
                        if (x - contours_g.at(gi).x<0)
                            pos = 1;
                        if (y - contours_g.at(gi).y < 0)
                            pos += 10;
                        vector<cv::Point2f>tgtPt;
                        tgtPt.push_back(hrect.tl());
                        tgtPt.push_back(Point2f(hrect.br().x, hrect.tl().y));
                        tgtPt.push_back(hrect.br());
                        tgtPt.push_back(Point2f(hrect.tl().x, hrect.br().y));
                        DrawTransPinP(src_img, hukidasi_img, src_img, tgtPt, pos);
                        */
                    }
                }
            }
            count = 0;
        }
    }
}

vector<Balloon*> GrobalBalloons;

#define LayoutMargen 5
#define BORDERGROW 10
Mat Process(Mat Source){
    /*
     //cvtColor(Source, Source, COLOR_BGR2GRAY);
     Source = cv::Scalar::all(255) - Source;
    //make eroded image
    Mat eroded;
    erode(Source, eroded, Mat(7, 7, CV_8U, cv::Scalar(1)));
    blur(eroded, eroded, Size(5, 5));
    //binalize
    Source = Source - eroded;
    threshold(Source, Source, 30, 1, CV_THRESH_BINARY);
    */
    
    
    Mat Thin;
    Source.copyTo(Thin);
    //thinning
    Thin2(Thin);
    //imshow("thin", Thin*255);
    
    //vectorize
    vector<Node*> ChainHeads;
    vector<Segment*> Segments;
    vector<Group*> Groups;
    //trace and make nodes
    Mat* clonemap = new Mat();
    Thin.copyTo(*clonemap);
    while (Trace(FirstPoint(*clonemap), clonemap, NULL, &ChainHeads, &Thin));
    //erase unnecessary nodes
    for (int i = 0; i < ChainHeads.size(); i++)
        ReducePath(ChainHeads[i]);
    //make vector segments
    for (int i = 0; i < ChainHeads.size(); i++)
        MakeSegment(ChainHeads[i], &Segments);
    //make groups
    for(Segment* seg : Segments)
        Groups.push_back(new Group(seg));
    while(MargeGroups(&Groups));
    for(int i = Groups.size()-1; i >= 0; i--)
        if(Groups[i] == NULL)
            Groups.erase(Groups.begin()+i);
    
    //make border image
    Mat BorderBase = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    for(Group* g : Groups)
        fillConvexPoly(BorderBase, g->BorderPath, Scalar(1));
	//imshow("borderbase", BorderBase * 255);
    Mat BorderErode = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    erode(BorderBase, BorderErode, Mat(BORDERGROW, BORDERGROW, CV_8U, cv::Scalar(1)));
    for(Segment* s : Segments)
        s->CheckBorder(BorderErode);
    Mat BorderImage = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    for(Segment* s : Segments)
        if(s->Curve()< 0.05 || s->isBorder)
            s->PrintLine(BorderImage);
    dilate(BorderImage, BorderImage, Mat(BORDERGROW, BORDERGROW, CV_8U, cv::Scalar(1)));
    BorderImage = BorderBase - BorderImage;
    //imshow("borderedge", BorderImage*255);
    Canny(BorderImage, BorderImage, 1, 1);
    //vectorize border
    vector<Node*> BorderHeads;
    vector<Segment*> BorderSegments;
    vector<Group*> BorderGroups;
    Mat BorderVec = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    while (Trace(FirstPoint(BorderImage), &BorderImage, NULL, &BorderHeads, NULL));
    for(int i = 0; i < BorderHeads.size(); i++)
        ReducePath(BorderHeads[i]);
    for(int i = 0; i < BorderHeads.size(); i++)
        MakeSegment(BorderHeads[i], &BorderSegments);
    for(Segment* s : BorderSegments)
        BorderGroups.push_back(new Group(s));
    while (MargeGroups(&BorderGroups));
    for(int i = 0; i < BorderGroups.size(); i++)
        if(BorderGroups[i] != NULL)
            if(contourArea(BorderGroups[i]->BorderPath) < 5000){
                delete BorderGroups[i];
                BorderGroups[i] = NULL;
            }
    for(int i = BorderGroups.size()-1; i >= 0; i--)
        if(BorderGroups[i] == NULL)
            BorderGroups.erase(BorderGroups.begin()+i);
    for(Group* g : BorderGroups)
        fillConvexPoly(BorderVec, g->BorderPath, Scalar(1));
    //check border
    for(Segment* s : Segments)
        s->CheckBorder(BorderVec);
    BorderVec = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    
    vector<vector<Point>> Layouts;
    for(Group* g : BorderGroups){
        vector<Point> formated;
        convexHull(FormatPath(g->BorderPath, 20),formated);
        if(formated.empty())continue;
        Layouts.push_back(GrowPath(formated, LayoutMargen));
    }
    vector<Point> LayoutOneLine;
    for(vector<Point> line : Layouts)
        LayoutOneLine.insert(LayoutOneLine.end(), line.begin(),line.end());
    LayoutOneLine = FormatPath(LayoutOneLine, 20);
    for(int i = 0; i < Layouts.size(); i++)
        for(int j = 0; j < Layouts[i].size(); j++){
            Layouts[i][j] = LayoutOneLine[0];
            LayoutOneLine.erase(LayoutOneLine.begin());
        }
    for(vector<Point> pts : Layouts)
        fillConvexPoly(BorderVec, GrowPath(pts, -LayoutMargen), Scalar(1));
    
    //--------get characters------------------
    Mat Chara;
    Source.copyTo(Chara);
    dilate(Chara, Chara,  Mat(5, 5, CV_8U, cv::Scalar(1)));
    erode(Chara, Chara,  Mat(5, 5, CV_8U, cv::Scalar(1)));
    erode(Chara, Chara,  Mat(5, 5, CV_8U, cv::Scalar(1)));
    dilate(Chara, Chara,  Mat(5, 5, CV_8U, cv::Scalar(1)));
    Mat CharaPlot;
    cvtColor(Source, CharaPlot, CV_GRAY2RGB);
    CharaPlot*=255;
    vector<vector<Balloon*>> Balloons;
    for(int gid = 0; gid < BorderGroups.size(); gid++){
        vector<Point2f> CharaPoints;
        for(Segment* s : Segments){
            if(s->Head->Chains != 1 && s->End->Chains != 1) continue;
            if(norm(s->Head->Position-s->End->Position)>10)continue;
            if(s->isBorder)continue;
            Point2f pt = (s->Head->Position+s->End->Position)*0.5;
            if(Chara.data[Chara.elemSize()*(int)(pt.x)+Chara.step*(int)(pt.y)] == 0)continue;
            if(pointPolygonTest(BorderGroups[gid]->BorderPath, pt, false) < 0)continue;
            CharaPoints.push_back(pt);
        }
        vector<Point2f> CharaPoints2;
        for(Point2f pt : CharaPoints){
            double val=0;
            for(Point2f pt2 : CharaPoints){
                double n = norm(pt-pt2);
                if(n>30)continue;
                val+=1;
            }
            if(val <= 5)continue;
            CharaPoints2.push_back(pt);
        }
        vector<Balloon*> LocalBalloons;
        if(CharaPoints2.size() < 5){
            Balloons.push_back(LocalBalloons);
            continue;
        }
        vector<int> labels;
        kmeans(CharaPoints2, 5, labels, TermCriteria(TermCriteria::MAX_ITER+TermCriteria::EPS, 50, FLT_EPSILON), 10, KMEANS_PP_CENTERS);
        for(int i = 0; i < 5; i++)
            LocalBalloons.push_back(new Balloon());
        for(int i = 0; i < CharaPoints2.size(); i++)
            LocalBalloons[labels[i]]->Points.push_back(CharaPoints2[i]);
        for(int i = 0; i < LocalBalloons.size(); i++){
            if(LocalBalloons[i]->Points.size() == 0){
                delete LocalBalloons[i];
                LocalBalloons[i] = NULL;
                continue;
            }
            LocalBalloons[i]->SetStd();
        }
        MargeLocalBalloons(&LocalBalloons);
        MargeLocalBalloons(&LocalBalloons);
        for(int i = 0; i < LocalBalloons.size(); i++){
            if(LocalBalloons[i] == NULL)continue;
            if((LocalBalloons[i]->StdX+LocalBalloons[i]->StdX) < 10)continue;
            double val = (double)LocalBalloons[i]->Points.size()/(LocalBalloons[i]->StdX+LocalBalloons[i]->StdX);
            if(val < 0.6)continue;
            for(Point2f pt : LocalBalloons[i]->Points)
                circle(CharaPlot, pt, 2, Scalar(i*30,0,255-i*30) ,2);
            ellipse(CharaPlot, LocalBalloons[i]->Avg, Size(LocalBalloons[i]->StdX*2,LocalBalloons[i]->StdY*2), 0, 0, 360, Scalar(0,255,255),2);
            vector<Segment*> NearSegments;
            for(Segment* s : Segments){
                if(s->isBorder)continue;
                if(s->AABB.width < 30 && s->AABB.height < 30)continue;
                double dx = (s->AABB.x+s->AABB.width/2)-LocalBalloons[i]->Avg.x;
                double dy = (s->AABB.y+s->AABB.height/2)-LocalBalloons[i]->Avg.y;
                dx /= LocalBalloons[i]->StdX;
                dy /= LocalBalloons[i]->StdY;
                if(sqrt(dx*dx+dy*dy) > 4)continue;
                NearSegments.push_back(s);
            }
            Group* NearGroup = NULL;
            for(Segment* s : NearSegments){
                bool headcol = false, endcol = false;
                for(Segment* s2 : NearSegments){
                    if(!headcol){
                        Point2f v1 = s2->End->Position-s2->Head->Position;
                        Point2f v2 = s->Head->Position-s2->Head->Position;
                        Point2f v3 = LocalBalloons[i]->Avg-s2->Head->Position;
                        double cross1 = v1.cross(v2);
                        double cross2 = v1.cross(v3);
                        if(cross1*cross2 < 0) headcol=true;
                    }
                    if(!endcol){
                        Point2f v1 = s2->End->Position-s2->Head->Position;
                        Point2f v2 = s->End->Position-s2->Head->Position;
                        Point2f v3 = LocalBalloons[i]->Avg-s2->Head->Position;
                        double cross1 = v1.cross(v2);
                        double cross2 = v1.cross(v3);
                        if(cross1*cross2 < 0) endcol=true;
                    }
                }
                if(headcol&&endcol)continue;
                if(NearGroup == NULL)
                    NearGroup = new Group(s);
                else
                    NearGroup->Marge(new Group(s));
                s->Print(CharaPlot, 1, Point2f(0,0));
            }
            vector<Point2f> BountPoints;
            Point2f StdVec = Point2f(LocalBalloons[i]->StdX,LocalBalloons[i]->StdY)*2;
            BountPoints.push_back(LocalBalloons[i]->Avg-StdVec);
            BountPoints.push_back(LocalBalloons[i]->Avg+StdVec);
            if(NearGroup!=NULL){
                //rectangle(CharaPlot, NearGroup->BB, Scalar(255,255,0));
                BountPoints.push_back(Point2f(NearGroup->BB.x,NearGroup->BB.y));
                BountPoints.push_back(Point2f(NearGroup->BB.x+NearGroup->BB.width,NearGroup->BB.y+NearGroup->BB.height));
            }
            //rectangle(CharaPlot, boundingRect(BountPoints), Scalar(255,255,0),2);
            LocalBalloons[i]->BB = boundingRect(BountPoints);
        }
        LocalBalloons.erase(std::remove(LocalBalloons.begin(), LocalBalloons.end(), (Balloon*)NULL), LocalBalloons.end());
        Balloons.push_back(LocalBalloons);
    }
    for(vector<Balloon*> blist : Balloons)
        for(Balloon* b : blist)
            rectangle(CharaPlot, b->BB, Scalar(255,255,0),3);
    //imshow("character", CharaPlot);
    
    //--------show images---------------------
    Thin = Thin.mul(255);
    cvtColor(Thin, Thin, COLOR_GRAY2BGR);
    
    Mat vecimage = Mat::zeros(Thin.rows, Thin.cols, CV_8UC3);
    Mat output = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
    for(Segment* g : Segments){
        g->Print(vecimage,1,Point(-0,-0));
        //if(g->isBorder)continue;
        g->PrintLine(output);
    }
    output = output.mul(BorderVec*200);
    Mat LayoutImage;
    Mat BalloonImage;
    for(int i = 0; i < Layouts.size(); i++){
        LayoutImage = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
        BalloonImage = Mat::zeros(Thin.rows, Thin.cols, CV_8U);
        fillConvexPoly(LayoutImage, GrowPath(Layouts[i], -LayoutMargen), Scalar(1));
        if(Balloons.size() > i){
            //Balloons[i].clear();
            for(Balloon* b : GrobalBalloons)
                Balloons[i].push_back(b);
            for(Balloon* b : Balloons[i]){
                Point2f cent = Point2f(b->BB.x+b->BB.width/2,b->BB.y+b->BB.height/2);
                Point2f LayoutMean = Point2f(0,0);
                for(Point2f pt : Layouts[i]) LayoutMean += pt;
                LayoutMean *= (double)1/Layouts[i].size();
                vector<Point2f> TargetPoints;
                Point2f p0 = b->BB.tl();
                Point2f p2 = b->BB.br();
                Point2f p1 = Point2f(p2.x, p0.y);
                Point2f p3 = Point2f(p0.x, p2.y);
                TargetPoints.push_back(p0);
                TargetPoints.push_back(p1);
                TargetPoints.push_back(p2);
                TargetPoints.push_back(p3);
                Mat dst;
                int pos=0;
                if(cent.x > LayoutMean.x)pos+=1;
                if(cent.y > LayoutMean.y)pos+=10;
                DrawTransPinP(dst, BalloonImg, BalloonImage, TargetPoints, pos);
                BalloonImage = dst;
            }
        }
        BalloonImage = BalloonImage.mul(LayoutImage);
        output = output+BalloonImage*255;
    }
    Canny(BorderVec, BorderVec, 1, 1);
    dilate(BorderVec, BorderVec, Mat(3, 3, CV_8U, cv::Scalar(1)));
    output = output + BorderVec;
    output = Scalar(255) - output;
    
    Mat bordertest = Mat::zeros(Thin.rows, Thin.cols, CV_8UC3);
    for(Group* g : BorderGroups)
        polylines(bordertest, g->BorderPath, true, Scalar(255,0,255));
    
    
    //cv::namedWindow("image", CV_WINDOW_AUTOSIZE | CV_WINDOW_FREERATIO);
    //cv::imshow("image", Thin);
    //cv::imshow("vector", vecimage*255);
    //imshow("output", output);
    //imshow("test", bordertest);
    return output;
}

#define BinaryThreshold 0.2
Mat FrameProc(Mat target){
    cvtColor(target, target, CV_BGR2GRAY);
    target = cv::Scalar::all(255) - target;
    Mat eroded;
    erode(target, eroded, Mat(7, 7, CV_8U, cv::Scalar(1)));
    blur(eroded, eroded, Size(5, 5));
    target = target - eroded;
    double min, max;
    minMaxLoc(target, &min, &max);
    threshold(target, target, min*(1-BinaryThreshold)+max*BinaryThreshold, 1, CV_THRESH_BINARY);
    line(target, Point(0,0), Point(target.cols, 0), Scalar(0));
    return target;
}

#define ASPECT 0.7
#define SIZE 400
int main(int argc, char *argv[]) {
    BalloonImg = imread("Balloon.png", IMREAD_UNCHANGED);
    if(BalloonImg.empty()){
        std::cout << "BalloonImg is empty" << endl;
        return -1;
    }
    if(BalloonImg.channels() != 1)
        cvtColor(BalloonImg, BalloonImg, CV_RGBA2GRAY);
    
    const char* filename = argc >= 2 ? argv[1] : "";
    while(argc >= 2){
        //load image
        Mat Source = imread(filename, 1);
        if (Source.empty()) {
            std::cout << "Source is empty" << endl;
            return -1;
        }
        hukidasi(Source, &GrobalBalloons);
        Source = FrameProc(Source);
        imshow("image", Process(Source));
        waitKey(100);
    }
    
    VideoCapture cap;
    bool isZeroCam = false;
    for(int i = 0;; i++){
        VideoCapture lcap(i);
        if(!lcap.isOpened()){
            isZeroCam = i==1;
            break;
        }
        cap = lcap;
    }
    if(!cap.isOpened()){
        cout << "camera open failed" << endl;
        return -1;
    }
    namedWindow("camera",1);
    Mat frame;
    cap >> frame;
    Rect PageFrame = Rect(0,0,frame.rows*ASPECT,frame.rows);
    PageFrame.x = frame.cols/2 - frame.rows*ASPECT/2;
    while(1)
    {
        cap >> frame;
        frame = frame(PageFrame);
        Mat frameview = FrameProc(frame);
        if(isZeroCam)flip(frameview, frameview, 1);
        imshow("camera", frameview*255);
        //imshow("camera", frame);
        if(waitKey(30) >= 0){
            resize(frame, frame, Size(SIZE, (int)(SIZE/(double)ASPECT)));
            hukidasi(frame, &GrobalBalloons);
            imshow("image", Process(FrameProc(frame)));
        }
    }
    return 0;
}
