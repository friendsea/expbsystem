#include "opencv2\\opencv.hpp"
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <time.h>
#define RESIZE_METHOD INTER_NEAREST
#define MATCHING_SIZE 256
#define MATCH_BORDER 0.8

using namespace std;
using namespace cv;

#pragma region
enum NodeType
{
	End,
	ThreeBranch,
	FourBranch,
	Path,
	Non
};

class Node
{
public:
	Node(int x, int y, NodeType type);
	NodeType Type;
	cv::Point Position;
	Node* Parent = NULL;
	vector<Node*> Child;
	void Print(Mat image);
};

Node::Node(int x, int y, NodeType type)
{
	Position.x = x;
	Position.y = y;
	Type = type;
}

void Node::Print(Mat image) {
	if (Type == Path)
		circle(image, Position, 2, Scalar(255, 0, 0));
	else
		circle(image, Position, 2, Scalar(0, 0, 255));
	if (Parent != NULL)
		line(image, Parent->Position, Position, Scalar(0, 255, 0));
	for (Node* n : Child)n->Print(image);
}
#pragma endregion

class Group
{
public:
	Group(Node* head, int childIndex);
	~Group();
	Node* Head;
	Node* End;
private:

};

Group::Group(Node* head, int childIndex)
{
	
}

Group::~Group()
{
}

void SetNeighbor(Mat src, int x, int y, int pixels[]) {
	pixels[0] = src.data[src.elemSize()*(x - 1) + src.step*(y - 1)] != 0;
	pixels[1] = src.data[src.elemSize()*(x - 1) + src.step*(y + 0)] != 0;
	pixels[2] = src.data[src.elemSize()*(x - 1) + src.step*(y + 1)] != 0;
	pixels[3] = src.data[src.elemSize()*(x + 0) + src.step*(y + 1)] != 0;
	pixels[4] = src.data[src.elemSize()*(x + 1) + src.step*(y + 1)] != 0;
	pixels[5] = src.data[src.elemSize()*(x + 1) + src.step*(y + 0)] != 0;
	pixels[6] = src.data[src.elemSize()*(x + 1) + src.step*(y - 1)] != 0;
	pixels[7] = src.data[src.elemSize()*(x + 0) + src.step*(y - 1)] != 0;
}

Point FirstPoint(Mat map) {
	for (int x = 1; x < map.cols - 2; x++)
		for (int y = 1; y < map.rows - 2; y++)
			if (map.data[map.step*y + map.elemSize()*x])
				return Point(x, y);
	return Point(0, 0);
}

bool ThinPixel2(Mat src, int x, int y, bool conditionchange) {
	if (src.data[src.elemSize()*x + src.step*y] == 0)return false;
	int pixels[8];
	SetNeighbor(src, x, y, pixels);
	int n = 0, d = 0;
	for (int i = 0; i < 8; i++) {
		n += pixels[i];
		d += (!pixels[i]) && pixels[(i + 1) % 8];
	}
	if (n < 2)return false;
	if (n > 6)return false;
	if (d != 1)return false;
	if (pixels[1] * pixels[3] * pixels[5 + conditionchange * 2])return false;
	if (pixels[3 - conditionchange * 2] * pixels[5] * pixels[7])return false;
	return true;
}

void Thin2(Mat src) {
	bool flag = false;
	Mat deletelist = Mat::zeros(src.rows, src.cols, CV_8U);
	for (int x = 1; x < src.cols - 1; x++)
		for (int y = 1; y < src.rows - 1; y++)
			flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, false));
	src = src - deletelist;
	if (!flag)return;

	deletelist = Scalar::all(0);
	for (int x = src.cols - 2; x > 0; x--)
		for (int y = src.rows - 2; y > 0; y--)
			flag |= (deletelist.data[y*deletelist.step + x*deletelist.elemSize()] = ThinPixel2(src, x, y, true));
	src = src - deletelist;
	if (flag)Thin2(src);
}

//vector<Node*> Nodes;
vector<Node*> ChainHeads;

bool Trace(Point pt, Mat* map, Node* node) {
	if (map->data[map->step*pt.y + map->elemSize()*pt.x] == 0)
		return false;
	map->data[map->step*pt.y + map->elemSize()*pt.x] = 0;
	Node* newnode = new Node(pt.x, pt.y, Path);
	//Nodes.push_back(newnode);
	if (node != NULL) {
		node->Child.push_back(newnode);
		newnode->Parent = node;
	}
	else ChainHeads.push_back(newnode);
	int branch = node != NULL;
	branch += Trace(Point(pt.x - 1, pt.y), map, newnode);
	branch += Trace(Point(pt.x, pt.y + 1), map, newnode);
	branch += Trace(Point(pt.x + 1, pt.y), map, newnode);
	branch += Trace(Point(pt.x, pt.y - 1), map, newnode);
	branch += Trace(Point(pt.x - 1, pt.y - 1), map, newnode);
	branch += Trace(Point(pt.x - 1, pt.y + 1), map, newnode);
	branch += Trace(Point(pt.x + 1, pt.y + 1), map, newnode);
	branch += Trace(Point(pt.x + 1, pt.y - 1), map, newnode);
	if (branch == 0)newnode->Type = Non;
	if (branch == 1)newnode->Type = End;
	if (branch == 3)newnode->Type = ThreeBranch;
	if (branch == 4)newnode->Type = FourBranch;
	return true;
}

Node* GetForwardPath(Node* node, int n) {
	if (n == 0)return node;
	if (node->Type != Path)return node;
	return GetForwardPath(node->Child[0], n - 1);
}

void ReducePath(Node* node) {
	//if (node->Type == Non)return;
	if (node->Type != Path ||
		node->Parent == NULL //||
		//currntlength >= 10
		) {
		for (int i = 0; i < node->Child.size(); i++) {
			//if (node->Chain[i] == beforenode)continue;
			ReducePath(node->Child[i]);
		}
		return;
	}
	//以下の処理に入るときは必ずType==Path
	//Chainは2こだけ
	Point v1 = node->Position - node->Parent->Position;
	Point v2 = GetForwardPath(node, 5)->Position - node->Parent->Position;
	double curve = abs(v1.x*v2.y - v1.y*v2.x) / norm(v2);
	if (curve > 1.1) {
		ReducePath(node->Child[0]);
		return;
	}
	//不要なノードなので前後を連結
	for (int i = 0; i < node->Parent->Child.size(); i++)
		if (node->Parent->Child[i] == node)
			node->Parent->Child[i] = node->Child[0];
	node->Child[0]->Parent = node->Parent;
	ReducePath(node->Child[0]);
	delete node;
}

int main(int argc, char *argv[]) {
	const char* filename = argc >= 2 ? argv[1] : "C:\\Users\\sea\\Desktop\\source (2).png";
	//元画像を読んで階調反転
	Mat Source = imread(filename, 1);
	if (Source.empty()) {
		std::cout << "Source is empty" << endl;
		return -1;
	}
	cvtColor(Source, Source, COLOR_BGRA2GRAY);
	Source = cv::Scalar::all(255) - Source;
	//収縮して線を消した画像を作る
	Mat eroded;
	erode(Source, eroded, Mat(7, 7, CV_8U, cv::Scalar(1)));
	blur(eroded, eroded, Size(5, 5));
	//線のみ残して二値化
	Source = Source - eroded;
	threshold(Source, Source, 60, 1, CV_THRESH_BINARY);
	//cv::imshow("bin", Source * 255);
	Thin2(Source);

	Mat* clonemap = new Mat();
	Source.copyTo(*clonemap);

	while (Trace(FirstPoint(*clonemap), clonemap, NULL));
	for (int i = 0; i < ChainHeads.size(); i++)
		ReducePath(ChainHeads[i]);

	Source = Source.mul(255);
	cvtColor(Source, Source, COLOR_GRAY2BGR);

	Mat vecimage = Mat::zeros(Source.rows, Source.cols, CV_8UC3);
	for (Node* n : ChainHeads)
		n->Print(vecimage);

	//cv::namedWindow("image", CV_WINDOW_AUTOSIZE | CV_WINDOW_FREERATIO);
	cv::imshow("image", Source);
	cv::imshow("vector", vecimage);
	cv::waitKey(0);
	return 0;
}
